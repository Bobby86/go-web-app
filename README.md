# README #

demo web application on Go
includes database usage part, user authentication, demo of front-end page etc.

### What is this repository for? ###

* demonstration

### How do I get set up? ###

* requires mysql database users with user table
* requires mysql driver github.com/go-sql-driver/mysql
* set your database connection settings at ./dbpackage/mydb.go init function
* requires sessions framework github.com/gorilla/sessions