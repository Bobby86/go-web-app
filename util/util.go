package util

import (
	"crypto/md5"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	loginPassError = "The login and password's first character must be a letter, it must contain at least 4 characters and no more than 15 characters and no characters other than letters, numbers and the underscore may be used"
)

var validLoginPass = regexp.MustCompile(`^[a-zA-Z]\w{3,14}$`)

// input user information sanitization and assertion
func UserInfoSanitization(username, password string) (string, string, error) {
	username = strings.TrimSpace(username)
	password = strings.TrimSpace(password)

	if ok := validLoginPass.MatchString(username); !ok {
		err := fmt.Errorf(loginPassError)
		return "", "", err
	}

	if ok := validLoginPass.MatchString(password); !ok {
		err := fmt.Errorf(loginPassError)
		return "", "", err
	}

	return username, password, nil
}

// generate unique form session-token
func GetNewToken() string {
	crutime := time.Now().Unix()
	h := md5.New()
	io.WriteString(h, strconv.FormatInt(crutime, 10))
	token := fmt.Sprintf("%x", h.Sum(nil))

	return token
}
