package util

import (
	"fmt"

	"golang.org/x/crypto/scrypt"
)

const (
	pW_SALT_BYTES = 32
	pW_HASH_BYTES = 32
	sALT          = "dsfsgsdplfwepflwfslkdncwuebdcuqobciqsgidhqgoufhnqkljfsbkeufbwuqbcq"
)

func EncryptPassword(uname, pass string) (string, error) {
	salt := make([]byte, pW_SALT_BYTES)
	addStr := uname[len(uname)/2:] + sALT
	copy(salt[:], addStr)
	// fmt.Println("str: ", addStr)
	// fmt.Println("slt: ", salt)
	hash, err := scrypt.Key([]byte(pass), salt, 16384, 8, 1, pW_HASH_BYTES) // returns []uint
	if err != nil {
		fmt.Print("pass hash key error")
		return "", err
	}
	s := []rune(string(hash[:]))
	// outStr := uint8ArrayToString(hash)
	// fmt.Println("hash: ", string(s))
	// fmt.Printf("type: %T\n", )
	return string(s), nil
}

// key value from database and uname, pass from request/sess
func ComparePassword(key, uname, pass string) (bool, error) {
	if key == "" || uname == "" || pass == "" {
		err := fmt.Errorf("util.ComparePassword: empty parameters's been passed")
		return false, err
	}
	hash, err := EncryptPassword(uname, pass)
	if err != nil {
		return false, err
	}
	// log.Printf("COMP: %x\n", key)
	// log.Printf("COMP: %x\n", hash)
	if key != hash {
		return false, nil
	}
	return true, nil
}
