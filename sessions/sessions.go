package sessions

import (
	"net/http"

	"github.com/gorilla/sessions"
)

var Store = sessions.NewCookieStore([]byte("secret-password")) //cookie session data storage
var session *sessions.Session

func IsLoggedIn(r *http.Request) bool {
	session, err := Store.Get(r, "session")
	if err == nil && (session.Values["loggedin"] == "true") {
		return true
	}
	return false
}

func GetCurrentUserName(r *http.Request) string {
	session, err := Store.Get(r, "session")
	if err != nil {
		return ""
	}
	return session.Values["username"].(string)
}
