package dbpackage

import (
	"database/sql"
	"log"

	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/vladimirigorevitch/main/util"
)

var database Database

type Database struct {
	db *sql.DB
}

// "GetDB: get database reference"
func GetDB() Database {
	return database
}

func init() {
	// defer database.db.Close()
	// connection to db
	db, err := sql.Open("mysql", "root:pass@/users?charset=utf8")
	checkErr(err)
	database.db = db
	log.Print("Successfuly connected to a database", database.db)
}

func Close() {
	database.db.Close()
}

func ValidUser(uname, pass string) bool {
	var passFromDb string
	log.Print("validating user: ", uname)
	rows, err := database.db.Query("SELECT password FROM user WHERE login=?", uname)
	checkErr(err) // TO refactor

	defer rows.Close()
	if rows.Next() {
		err := rows.Scan(&passFromDb)
		if err != nil {
			log.Print("rows.Scan for password was failed for: ", uname)
			return false
		}
	}

	match, err := util.ComparePassword(passFromDb, uname, pass)
	if err != nil {
		fmt.Print("got error comparing passwords")
		return false
	} else if match {
		log.Print("user are valid")
		return true
	}

	log.Print("user not found")
	return false
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}

func AddUser(uname string, pass string) error {
	log.Print("Add user called")
	// check if user with the same login already exist
	//query
	rows, err := database.db.Query("SELECT login FROM users.user") // WHERE login=?", uname)
	// if driverErr, ok := err.(*mysql.MySQLError); ok {
	// 	if driverErr.Number == 1045 {
	// 		log.Fatal ("mysql: permission denied")
	// 	}
	// }
	defer rows.Close()
	if err != nil {
		log.Print("AddUser failure", err.Error())
		return err
	}

	for rows.Next() {
		var loginField string //TODO sanitize
		err = rows.Scan(&loginField)
		if err != nil {
			log.Print("AddUser failure", err.Error())
			return err
		}
		if loginField == uname {
			log.Printf("username with %q already exist", uname)
			err := fmt.Errorf("username with %q already exist", uname)
			log.Print("AddUser failure", err.Error())
			return err
		}
		log.Print("user login" + uname + " not found in the database")
	}
	//prepare statement
	statement, err := database.db.Prepare("INSERT user SET login=?, password=?")
	if err != nil {
		log.Print("AddUser failure", err.Error())
		return err
	}
	// encrypt password
	pass, err = util.EncryptPassword(uname, pass)
	if err != nil {
		log.Print("AddUser failure: password encryption falure", err.Error())
		return err
	}
	//execute statement
	res, err := statement.Exec(uname, pass)
	if err != nil {
		log.Print("AddUser failure", err.Error())
		return err
	}

	_, err = res.LastInsertId()
	// log.Print(id)
	if err != nil {
		log.Print("AddUser failure", err.Error())
		return err
	}
	log.Printf("exit from AddUser")

	return nil
}
