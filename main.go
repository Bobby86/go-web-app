package main

import (
	"log"
	"net/http"
	"os"

	"github.com/vladimirigorevitch/main/dbpackage"
	"github.com/vladimirigorevitch/main/views"
)

func main() {
	views.PopulateTemplates()

	fs := http.FileServer(http.Dir("public"))
	// http.Handle("/", fs)
	http.Handle("/static/owls/", fs)
	http.Handle("/public/", http.StripPrefix("/public/", fs))

	http.HandleFunc("/login/", views.LoginFunc)
	http.HandleFunc("/signup/", views.SignUpFunc)
	http.HandleFunc("/logout/", views.LogoutFunc)
	http.HandleFunc("/", views.HomePage)

	// database stuff
	dbpackage.AddUser("Admin", "pass")
	// run the server
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	// log.Print(port)
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Printf("error ListenAndServe: %q\n", err)
	}
}
