package views

import (
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/vladimirigorevitch/main/sessions"
)

var LoginTemplate *template.Template
var HomeTemplate *template.Template
var SignupTemplate *template.Template

func PopulateTemplates() {
	var allFiles []string
	templatesDir := "./template/"
	files, err := ioutil.ReadDir(templatesDir)
	if err != nil {
		log.Println(err)
		// http.Error(http.ResponseWriter, err.Error(), http.StatusInternalServerError)
		os.Exit(1)
	}
	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".html") || strings.HasSuffix(filename, ".gptl") {
			allFiles = append(allFiles, templatesDir+filename)
		}
	}
	templates, err := template.ParseFiles(allFiles...)
	if err != nil {
		log.Println(err)
		// http.Error(http.ResponseWriter, err.Error(), http.StatusInternalServerError)
		os.Exit(1)
	}
	LoginTemplate = templates.Lookup("login.html")
	HomeTemplate = templates.Lookup("home.html")
	SignupTemplate = templates.Lookup("register.html")
}

func HomePage(w http.ResponseWriter, r *http.Request) {
	var LoginName string
	if sessions.IsLoggedIn(r) {
		username := sessions.GetCurrentUserName(r)
		LoginName = "" + username
		log.Print("TEST0 ", LoginName)
	}
	err := HomeTemplate.Execute(w, LoginName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
