package views

import (
	"log"
	"net/http"

	"github.com/vladimirigorevitch/main/dbpackage"
	"github.com/vladimirigorevitch/main/sessions"
	"github.com/vladimirigorevitch/main/util"
)

var SignupErrorMessage string

type LoginMessages struct {
	ErrorMessage string
	Token        string
}

// ------ authentication part ----

// will be used for each HttpHandler
func RequiresLogin(handler func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if !sessions.IsLoggedIn(r) {
			http.Redirect(w, r, "/login/", 302)
			return
		}
		handler(w, r)
	}
}

// delete sess info from cookie store (Store)
func LogoutFunc(w http.ResponseWriter, r *http.Request) {
	session, err := sessions.Store.Get(r, "session")
	if err == nil {
		if session.Values["loggedin"] != false {
			session.Values["loggedin"] = "false"
			session.Values["username"] = ""
			session.Values["token"] = ""
			err = session.Save(r, w)
			log.Printf("%v\n", session)
			log.Println(err)
		}
	}
	http.Redirect(w, r, "/", 302)
}

// add a cookie to cookie store for managing authentication
func LoginFunc(w http.ResponseWriter, r *http.Request) {
	session, err := sessions.Store.Get(r, "session")
	if err != nil {
		log.Println("error identifying session")
		err := LoginTemplate.Execute(w, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	switch r.Method {
	case "GET":
		// create token, save it to the empty session, put it in a form
		token := util.GetNewToken()
		session.Values["token"] = token
		session.Save(r, w)
		message := LoginMessages{Token: token}
		err := LoginTemplate.Execute(w, message)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	case "POST":
		log.Print("Login: inside POST")
		r.ParseForm()
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		var message LoginMessages
		// validate token
		token := r.Form.Get("token")
		if token == "" || token != session.Values["token"] {
			// possible attack
			return
		}
		message.Token = token // else

		// sanitize form info
		username, password, err := util.UserInfoSanitization(username, password)
		if err != nil {
			message.ErrorMessage = err.Error()
			err = LoginTemplate.Execute(w, message)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			return
		}
		// check user validity
		if ok := dbpackage.ValidUser(username, password); ok {
			session.Values["loggedin"] = "true"
			session.Values["username"] = username
			session.Save(r, w)
			log.Printf("%v\n", session)
			log.Println("ValidUserError: ", err)
			log.Print("user ", username, " is authenticated")
			http.Redirect(w, r, "/", 302)
			return
		}
		log.Print("User is not valid " + username)
		message.ErrorMessage = "user name or password not valid"
		err = LoginTemplate.Execute(w, message)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	default:
		http.Redirect(w, r, "/login/", http.StatusUnauthorized)
	}
}

//--- registration part -----

func SignUpFunc(w http.ResponseWriter, r *http.Request) {
	session, err := sessions.Store.Get(r, "session")
	if err != nil {
		log.Println("error identifying session")
		err := LoginTemplate.Execute(w, nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}

	switch r.Method {
	case "GET":
		token := util.GetNewToken()
		session.Values["token"] = token
		session.Save(r, w)
		message := LoginMessages{Token: token, ErrorMessage: "Registration"}
		err := SignupTemplate.Execute(w, message)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	case "POST":
		var message LoginMessages // struct with error message and token
		r.ParseForm()
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		//sex := r.Form.Fet("gender")

		// token validation
		token := r.Form.Get("token")
		if token == "" || token != session.Values["token"] {
			// possible attack
			return
		}
		message.Token = token // else - reuse

		// sanitize form info
		username, password, err := util.UserInfoSanitization(username, password)
		if err != nil {
			message.ErrorMessage = err.Error()
			err = SignupTemplate.Execute(w, message)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			return
		}

		// save new user // or throw an error if user already exist
		err = dbpackage.AddUser(username, password)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			http.Redirect(w, r, "/login/", 302)
		}
		// } else {
		// 	// TODO error otput: login/pass incorrect
		// 	SignupErrorMessage = "incorrect login or password (must be longer than 3 characters)"
		// 	err := SignupTemplate.Execute(w, SignupErrorMessage)
		// 	if err != nil {
		// 		http.Error(w, err.Error(), http.StatusInternalServerError)
		// 	}
		// }
	default:
		http.Redirect(w, r, "/", http.StatusBadRequest)
	}
}
